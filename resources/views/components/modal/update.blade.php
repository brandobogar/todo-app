<!-- The button to open modal -->
<label for="my_modal_7" class="btn btn-sm btn-accent">Update</label>

<!-- Put this part before </body> tag -->
<input type="checkbox" id="my_modal_7" class="modal-toggle" />
<div class="modal">
    <div class="modal-box">
        <h3 class="font-bold text-lg">Update Todo</h3>
        <form action="{{ route('todos.update', $todo->id) }}" method="POST" class="form-control flex gap-y-4 mt-4"
            enctype="multipart/form-data">
          @method('PUT')
            @csrf

            <div>
                <label for="title">Title</label>
                <input type="text" name="title" id="title" placeholder="Title"  value="{{ old('title', $todo->title) }}"
                    class="input input-bordered w-full">
            </div>
            <div class="flex flex-row h-fit w-full items-center justify-start gap-x-4">
              
                <select class="select select-bordered w-full max-w-xs" id="status" name="status" >
                  <option disabled selected>Status</option>
                  <option value="Done">Done</option>
                  <option value="Not Done">Not Done</option>
                </select>
            </div>

            <div class="modal-action">
                <button class="btn btn-accent" type="submit">Save</button>
                <label for="my_modal_7" class="btn btn-error">Close!</label>
            </div>
        </form>

    </div>
</div>
