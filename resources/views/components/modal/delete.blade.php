<!-- The button to open modal -->
<label for="my_modal_{{ $todo->id }}" class="btn btn-sm btn-error">delete</label>

<!-- Put this part before </body> tag -->
<input type="checkbox" id="my_modal_{{ $todo->id }}" class="modal-toggle" />
<div class="modal">
    <div class="modal-box">
        <h3 class="font-bold text-lg">Delete Todo</h3>
        <p class="py-4">Are you sure to delete this todo?</p>
        <form action="{{ route('todos.destroy', $todo->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <div class="modal-action">
                <button type="submit" class="btn btn-accent">Yes</button>
                <label for="my_modal_{{ $todo->id }}" class="btn btn-error">No</label>
        </form>
    </div>
</div>
</div>
