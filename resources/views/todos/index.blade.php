<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    @vite('resources/css/app.css')


    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
    </style>
</head>

<body class="bg-amber-50 h-screen w-screen pt-20 flex items-center justify-start flex-col gap-4">

    <h1 class="font-semibold text-3xl">Todo list</h1>
    <div class="w-1/2">

        @include('components.modal.add')
    </div>
    <div class=" w-1/2 border-slate-100 border-2">
        <table class="table table-md">
            <thead class="text-lg">
                <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Created at</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>


                @forelse ($todos as $todo)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $todo->title }}</td>
                        <td>{{ $todo->status }}</td>
                        <td>{{ $todo->created_at }}</td>
                        <td>
                            <div class="flex gap-3">
                                {{-- @component('components.modal.update')
                                    
                                @endcomponent --}}
                                @include('components.modal.update')
                                @include('components.modal.delete')
                            </div>
                        </td>
                    </tr>


                @empty
                    <tr>
                        <td colspan="5">Tidak ada data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>

    </div>
</body>


</html>
