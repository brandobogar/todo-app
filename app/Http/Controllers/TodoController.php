<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    public function index()
    {
        // $todos  = Todo::latest()->get();
        $todos  = Todo::all();

        $no = 1;
        return view('todos.index', compact('todos', 'no'));
    }



    public function store(Request $request)
    {
        Todo::create([
            'title' => $request->title,
            'status' => $request->status
        ]);

        return redirect()->route('todos.index')->with('success', 'Todo Added Successfully');
    }

    public function edit(Todo $todo)
    {
        return view('component.modal.update', compact('todo'));
    }

    public function update(Request $request, Todo $todo)
    {
        $request->validate([
            'title' => 'required',
            'status' => 'required'
        ]);

        $todo->title = $request->title;
        $todo->status = $request->status;
        $todo->save();

        return redirect()->route('todos.index')->with('success', 'Todo Updated Successfully');
    }

    public function destroy(Todo $todo)
    {
        $todo->delete();
        return redirect()->route('todos.index')->with('success', 'Todo Deleted Successfully');
    }
}
